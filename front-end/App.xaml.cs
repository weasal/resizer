﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using System.Threading; // Mutex
using System.Diagnostics; // Process
using System.Runtime.InteropServices; // DllImport
using System.IO;
using DesktopToastsSample.ShellHelpers;
using Microsoft.WindowsAPICodePack.Shell.PropertySystem;
using MS.WindowsAPICodePack.Internal;

/// <summary>
/// Native functions used to inject a dll into Continuum
/// @see http://msdn.microsoft.com/en-us/library/26thfadc(v=vs.110).aspx
/// </summary>

public class NativeFunctions
{
    [DllImport("kernel32.dll")]
    public static extern IntPtr LoadLibrary(string lpFileName);

    [DllImport("kernel32.dll")]
    public static extern bool FreeLibrary(IntPtr hModule);

    [DllImport("kernel32.dll")]
    public static extern IntPtr GetProcAddress(IntPtr hModule, string lpProcName);

    [DllImport("user32.dll")]
    public static extern uint GetWindowThreadProcessId(IntPtr hWnd, IntPtr lpdwProcessId);

    public delegate int HookProc(int code, IntPtr wParam, IntPtr lParam);
    [DllImport("user32.dll")]
    public static extern int SetWindowsHookEx(int idHook, HookProc lpfn, IntPtr hMod, uint dwThreadId);
    
    [DllImport("user32.dll")]
    public static extern void UnhookWindowsHookEx(int idHook);

    [DllImport("injection.dll", CallingConvention = CallingConvention.StdCall)]
    public static extern void SetCustomResolution(int x, int y);
}

namespace resizer
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private Mutex resizerMutex;
        private Process continuumGame;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private SettingsWindow settingsWindow;
        private Thread settingsWindowThread;
        private const string INJECTION_LIBRARY_NAME = "injection.dll";
        private const string RESIZER_APP_ID = "Weasalss.Continuum.Resizer";

        private bool AcquireResizerMutex(out Mutex mutex)
        {
            bool mutexIsNew = true;
            mutex = new Mutex(true, "ContinuumResizerMutex", out mutexIsNew);
            if (!mutexIsNew)
            {   // if the mutex isn't new it means that an owner exists already,
                // if we restarted we should be able to successfully wait to acquire it
                if (!mutex.WaitOne(5000))
                {
                    mutex = null;
                    return false;
                }
            }

            return true;
        }

        private void ForceExit(string error = "")
        {
            if (error != "")
            {
                MessageBox.Show(error, "Continuum Resizer Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            if (notifyIcon != null)
            {
                DestroyNotifyIcon();
            }
            if (continuumGame != null && !continuumGame.HasExited)
            {
                //continuumGame.Kill();
                continuumGame.CloseMainWindow();
            }
            if (resizerMutex != null)
            {
                resizerMutex.ReleaseMutex();
            }
            Environment.Exit(0);
        }

        private string GetLaunchPath()
        {
            String launchPath = resizer.Properties.Settings.Default.ContinuumPath;
            if (launchPath == "")
            {
                this.MainWindow = new MainWindow();
                this.MainWindow.ShowDialog();
                launchPath = resizer.Properties.Settings.Default.ContinuumPath;
            }
            return launchPath;
        }

        private bool GetContinuumGameProcess(out Process continuumProcess)
        {
            continuumProcess = null;
            Process candidate;

            // If a process named continuum does not already exist run continuum
            Process[] processSearchResults = Process.GetProcessesByName("Continuum");
            if (processSearchResults.Length == 0)
            {
                /* Continuum.exe actually creates a process to unpack the game, then starts
                   another process to run the unpacked game. Thus, we wait for continuum to be unpacked, launched,
                   and then get the resulting process by confirming its window title. */
                try
                {
                    Process initialProcess = Process.Start(GetLaunchPath());

                    while (!initialProcess.HasExited)
                    {
                        Thread.Sleep(10);
                    }

                    int searchAttempts = 0;
                    const uint MAX_SEARCH_ATTEMPTS = 10;
                    while (searchAttempts < MAX_SEARCH_ATTEMPTS)
                    {
                        processSearchResults = Process.GetProcessesByName("Continuum");
                        if (processSearchResults.Length > 0)
                        {
                            candidate = processSearchResults[0];
                            if (candidate.MainWindowTitle == "Continuum 0.40")
                            {
                                continuumProcess = processSearchResults[0];
                                return true;
                            }
                        }
                        Thread.Sleep(10 * searchAttempts);
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else
            {
                candidate = processSearchResults[0];
                if (!candidate.HasExited && candidate.MainWindowTitle == "Continuum 0.40")
                {
                    continuumProcess = candidate;
                    return true;
                }
            }

            return false;
        }

        private void CreateNotifyIcon()
        {
            notifyIcon = new System.Windows.Forms.NotifyIcon();
            notifyIcon.Text = "Continuum Resizer";
            notifyIcon.Icon = resizer.Properties.Resources.NotificationIcon;

            notifyIcon.Visible = true;

            notifyIcon.BalloonTipTitle = "Continuum Resizer";
            notifyIcon.BalloonTipText = "Continuum resizer launched successfully.";
            notifyIcon.ShowBalloonTip(3000);

            notifyIcon.Click += NotifyIconClickHandler;
        }
     

        private void NotifyIconClickHandler(object sender, EventArgs e)
        {
            if (settingsWindow != null)
            {
                Func<bool> func = settingsWindow.Activate;
                Dispatcher.FromThread(settingsWindowThread).BeginInvoke(func);
            } else
            {
                settingsWindowThread = new Thread(new ThreadStart(SettingsWindowThread));
                settingsWindowThread.SetApartmentState(ApartmentState.STA);
                settingsWindowThread.IsBackground = true;
                settingsWindowThread.Start();
            }
        }

        private void SettingsWindowThread()
        {
            settingsWindow = new SettingsWindow();
            settingsWindow.Closed += HandleSettingsClose;
            settingsWindow.Show();
            System.Windows.Threading.Dispatcher.Run();
        }

        private void HandleSettingsClose(object sender, EventArgs e) {
            settingsWindow = null;

            var settings = resizer.Properties.Settings.Default;
            NativeFunctions.SetCustomResolution(settings.CustomResolutionX, settings.CustomResolutionY);
        }

        private void DestroyNotifyIcon()
        {
            notifyIcon.Visible = false;
            notifyIcon.Dispose();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            // Ensure this program only has one instance
            if (!AcquireResizerMutex(out resizerMutex))
                ForceExit("Please check that Continuum Resizer is not already running");

            // To be able to send notifications
            TryCreateShortcut();

            // Get continuum game process (continuum.exe is executed if it isn't running already)
            if (!GetContinuumGameProcess(out continuumGame))
            {
                // Can't launch continuum properly or find the window -- launchPath could be invalid so reset it
                resizer.Properties.Settings.Default.ContinuumPath = "";
                resizer.Properties.Settings.Default.Save();
                ForceExit("Could not launch or detect a Continuum window. Try running Continuum Resizer again.");
            }

            if (!InjectAndWaitForExit())
            {
                ForceExit("Injection error!");
            }

            resizerMutex.ReleaseMutex();
            Application.Current.Shutdown();
        }

        private bool InjectAndWaitForExit()
        {
            IntPtr dllHandle = NativeFunctions.LoadLibrary(INJECTION_LIBRARY_NAME);
            if (dllHandle == IntPtr.Zero)
                return false;

            // Use SetWindowsHookEx for dll injection
            IntPtr hookPtr = NativeFunctions.GetProcAddress(dllHandle, "CallWindowHook");
            if (hookPtr == IntPtr.Zero)
                return false;

            uint threadId = NativeFunctions.GetWindowThreadProcessId(continuumGame.MainWindowHandle, IntPtr.Zero);
            NativeFunctions.HookProc hookProcedure = (NativeFunctions.HookProc)
                Marshal.GetDelegateForFunctionPointer(hookPtr, typeof(NativeFunctions.HookProc));

            // value for WH_CALLWNDPROC is 4
            int hHook = NativeFunctions.SetWindowsHookEx(4, hookProcedure, dllHandle, threadId);
            if (hHook == 0)
                return false;

            var settings = resizer.Properties.Settings.Default;
            NativeFunctions.SetCustomResolution(settings.CustomResolutionX, settings.CustomResolutionY);

            // Create the tray icon
            CreateNotifyIcon();

            continuumGame.WaitForExit();

            DestroyNotifyIcon();

            NativeFunctions.UnhookWindowsHookEx(hHook);
            NativeFunctions.FreeLibrary(dllHandle);
            return true;
        }

        // Taken from Microsoft sample: https://code.msdn.microsoft.com/windowsdesktop/sending-toast-notifications-71e230a2
        // In order to display toasts, a desktop application must have a shortcut on the Start menu.
        // Also, an AppUserModelID must be set on that shortcut.
        // The shortcut should be created as part of the installer. The following code shows how to create
        // a shortcut and assign an AppUserModelID using Windows APIs. You must download and include the 
        // Windows API Code Pack for Microsoft .NET Framework for this code to function
        //
        // Included in this project is a wxs file that be used with the WiX toolkit
        // to make an installer that creates the necessary shortcut. One or the other should be used.
        private bool TryCreateShortcut()
        {
            String shortcutPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Microsoft\\Windows\\Start Menu\\Programs\\Continuum.lnk";
            if (!File.Exists(shortcutPath))
            {
                InstallShortcut(shortcutPath);
                return true;
            }
            return false;
        }

        private void InstallShortcut(String shortcutPath)
        {
            // Find the path to the current executable
            String exePath = GetLaunchPath();
            IShellLinkW newShortcut = (IShellLinkW)new CShellLink();

            // Create a shortcut to the exe
            VerifySucceeded(newShortcut.SetPath(exePath));
            VerifySucceeded(newShortcut.SetArguments(""));

            // Open the shortcut property store, set the AppUserModelId property
            IPropertyStore newShortcutProperties = (IPropertyStore)newShortcut;

            using (PropVariant appId = new PropVariant(RESIZER_APP_ID))
            {
                VerifySucceeded(newShortcutProperties.SetValue(SystemProperties.System.AppUserModel.ID, appId));
                VerifySucceeded(newShortcutProperties.Commit());
            }

            // Commit the shortcut to disk
            IPersistFile newShortcutSave = (IPersistFile)newShortcut;

            VerifySucceeded(newShortcutSave.Save(shortcutPath, true));
        }

        public static void VerifySucceeded(UInt32 hresult)
        {
            if (hresult > 1)
            {
                throw new Exception("Failed with HRESULT: " + hresult.ToString("X"));
            }
        }
    }
}
