﻿using System;
using System.Windows;
using System.Windows.Forms;

namespace resizer
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class SettingsWindow : Window
    {
        double scaleValue;
        int customX, customY;

        public SettingsWindow()
        {
            InitializeComponent();
            resolutionSlider.ValueChanged += OnScaleResolution;
            resolutionSlider.Value = Properties.Settings.Default.ResolutionSlider;
            applyButton.Click += OnApply;

            UpdateUI();
        }

        private double CalculateScaleValue()
        {
            // resolution scaling ranges from 0.5x to 2x
            return 0.5 + 1.5 * (resolutionSlider.Value / resolutionSlider.Maximum);
        }

        private void OnScaleResolution(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            scaleValue = CalculateScaleValue(); 
            customX = (int)(Screen.PrimaryScreen.Bounds.Width * scaleValue);
            customY = (int)(Screen.PrimaryScreen.Bounds.Height * scaleValue);

            UpdateUI();
        }

        private void OnApply(object sender, RoutedEventArgs e)
        {
            // Save preferences
            Properties.Settings.Default.ResolutionSlider = resolutionSlider.Value;
            Properties.Settings.Default.CustomResolutionX = customX;
            Properties.Settings.Default.CustomResolutionY = customY;
            Properties.Settings.Default.Save();

            Close();
        }

        private void UpdateUI()
        {
            resolutionText.Text = customX + " x " + customY + " (" + scaleValue.ToString("N2") + "×)";

            screenTransform.ScaleX = 2 / scaleValue;
            screenTransform.ScaleY = 2 / scaleValue;
        }
    }
}
