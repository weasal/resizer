# Continuum Resizer #
***
The goal of Continuum Resizer is to improve the performance and add some convenience to playing Continuum on modern versions of Windows. 
The screenshot below is the game running at 2844x1600 on a 1920x1080 resolution monitor using Continuum Resizer.

[![2844x1600 custom resolution on a 1920x1080 monitor](https://i.imgur.com/8d0InTth.png)](https://i.imgur.com/8d0InTt.png)

### [**Download resizer.zip**](https://bitbucket.org/weasal/resizer/downloads/resizer.zip) (built April 22nd 2018) ###


## Features ##
* Play Continuum at resolutions higher than your monitor's native resolution
* Fullscreen windowed mode (multi-monitor friendly and quick alt-tabbing)
* Direct2D renderer (better performance and possibly lower CPU/GPU power consumption)
* Semi-transparent radar
* Custom chat font, size, and anti-aliased text rendering
* Supports Windows 10 notifications for private messages when you're alt-tabbed

## History ##
Resizer has been tweaked over the years as I thought of new things I'd like. The tweaks mostly had to do with improving the usability of the program but also include the addition of some major features over time.
The initial version of Resizer (v1.0) was released in May 2012 (according to this [forum post](http://forums.trenchwars.org/showthread.php?43076-Continuum-Resizer-new-app-to-that-makes-playing-convenient)). 
That version simply made alt-tabbing less clunky on Windows 7 and also allowed Continuum to be played at arbitrary resolutions. A year later, I realized I could take the basic idea of Resizer further by doing all the game's drawing myself. 
This plan was motivated by Continuum's shitty performance and responsiveness when run at high (custom) resolutions on my laptop. Yes, the laptop had integrated graphics but Continuum is a *2d spaceship game*, it's not Crysis. 

I thought the poor performance was because Continuum uses a decades old drawing API known as [DirectDraw](https://en.wikipedia.org/wiki/DirectDraw). What's DirectDraw? DirectDraw is really old software that was designed pre-modern graphics cards. Instead, Microsoft
has a relatively new shiny 2D drawing library (known as Direct2D) on Windows Vista and beyond that properly leverages modern 3D graphics hardware. 
I was eager to see what the performance would be if I shifted Continuum to this new API (mostly because I thought higher FPS would give me a competitive advantage!).
So between August 2013 and the middle of 2014 I added mouse aim (i.e. your ship points to your mouse) and Direct2D rendering to Resizer and christened the new version Resizer2. 
After some time (about a year) I felt that this code was poor quality so I started a complete rewrite of Resizer, the progress of which is in this repo's commit history.

## How do I get set up? ##
To get started on developing or building Continuum Resizer, you must be using an OS >= Windows8.

* Download Microsoft Visual Studio Community 2017 for Windows Desktop
* Install Microsoft detours from [here](http://research.microsoft.com/en-us/downloads/d36340fb-4d3c-4ddd-bf5b-1db25d03713d/default.aspx)
* Build the detours library
* Add detours.lib to the project
* Checkout Microsoft's winrt headers [here](https://github.com/Microsoft/cppwinrt)
* Build the projects
* Before running, make sure injection.dll is in the same directory as resizer.exe

## I need help! Who do I talk to? ##
`?find` or `?message Crescendo` in game.
