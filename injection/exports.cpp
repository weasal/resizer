#include "stdafx.h"
#include "globals.h"
#include "exports.h"
#include "windowutil.h"
#include "continuummemory.h"

// Settings that are read in, using shared data segment so single use between
// all loaded dlls
#pragma data_seg("shared_data")
// 1920 x 1080 by default
int customX = 1920;
int customY = 1080;
#pragma data_seg()
#pragma comment(linker, "/section:shared_data,RWS") // set the data segment to shared in the linker

void ApplyCustomResolution(HWND uiWindowHandle) {
	unsigned int menuId = SetLastLauncherResolutionMenuEntry(uiWindowHandle, customX, customY);
	ContinuumMemory memory;
	memory.SetCustomResolution(menuId, customX, customY);
	// Finally, select the entry
	PostMessage(uiWindowHandle, WM_COMMAND, menuId, 0);
}

#define MAX_UI_WINDOW_WAIT 3000
extern "C" __declspec(dllexport) void __stdcall SetCustomResolution(int x, int y) {
	customX = x;
	customY = y;

	auto time = GetTickCount();
	HWND uiWindow;
	do {
		uiWindow = FindWindow(NULL, CONTINUUM_LAUNCHER_WINDOW_TITLE);
	} while (!uiWindow && (GetTickCount() - time < MAX_UI_WINDOW_WAIT));

	if (uiWindow) {
		ApplyCustomResolution(uiWindow);
	}
}

extern "C" __declspec(dllexport) int CallWindowHook(int code, WPARAM wParam, LPARAM lParam) {
	if (code < 0)  // do not process message 
		return CallNextHookEx(NULL, code, wParam, lParam);

	if (!initialized) {
#ifdef _DEBUG
		CreateConsole();
		printf("Attached to console.\n");
		printf("Process id: %i, thread id: %i\n", GetCurrentProcessId(), GetCurrentThreadId());
#endif
		initialized = true;
	}

	CWPSTRUCT* msg = (CWPSTRUCT *)lParam;
	CONTINUUM_WINDOW_TYPE windowType = getWindowType(msg->hwnd);
	if (windowType == CONTINUUM_WINDOW_TYPE::GAME_WINDOW) {
		switch (msg->message) {
		case WM_CREATE:
#ifdef USE_CUSTOM_CHAT
			chatwatcher = new chat::LogWatcher();
			chatcomposer = new chat::Composer();
#endif
			detour = new DirectDrawDetourer();
			break;
		case WM_DESTROY:
			delete detour;
			detour = nullptr;
#ifdef USE_CUSTOM_CHAT
			delete chatcomposer;
			chatcomposer = nullptr;
			delete chatwatcher;
			chatwatcher = nullptr;
#endif
			break; 
		/*case WM_ACTIVATE:
			if (msg->wParam == WA_INACTIVE)
				PostMessage(msg->hwnd, WM_SIZE, SIZE_MINIMIZED, 0);
			else
				PostMessage(msg->hwnd, WM_SIZE, SW_RESTORE, 0);*/
		}
	}
	else if (windowType == CONTINUUM_WINDOW_TYPE::LAUNCHER_WINDOW && msg->message == WM_ACTIVATE) {
		// Everytime we exit game its necessary to re-apply the custom res
		ApplyCustomResolution(msg->hwnd);
	}

	return CallNextHookEx(NULL, code, wParam, lParam);
}