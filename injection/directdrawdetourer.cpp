#include "stdafx.h"
#include "directdrawdetourer.h"

DirectDrawDetourer::DirectDrawFunctions DirectDrawDetourer::FUNCTIONS = DirectDrawDetourer::DirectDrawFunctions{};

DirectDrawDetourer::DirectDrawDetourer() {
	Detour::ResetData();
	updateDirectDrawFunctions();
	installDetours();
}

#define CREATE_SURFACE_CAST			(HRESULT(STDMETHODCALLTYPE*)(IDirectDraw FAR *, LPDDSURFACEDESC, LPDIRECTDRAWSURFACE FAR *, IUnknown FAR *))
#define SET_COOPERATIVE_LEVEL_CAST	(HRESULT(STDMETHODCALLTYPE *)(IDirectDraw FAR *, HWND, DWORD))
#define SET_DISPLAY_MODE_CAST		(HRESULT(STDMETHODCALLTYPE *)(IDirectDraw FAR *, DWORD, DWORD, DWORD))
#define SURFACE_BLT_CAST			(HRESULT(STDMETHODCALLTYPE *)(IDirectDrawSurface FAR *, LPRECT, LPDIRECTDRAWSURFACE, LPRECT, DWORD, LPDDBLTFX))
#define SURFACE_UNLOCK_CAST			(HRESULT(STDMETHODCALLTYPE *)(IDirectDrawSurface FAR *, LPVOID))
#define SURFACE_RELEASE_CAST		(ULONG(STDMETHODCALLTYPE *)(IDirectDrawSurface FAR *))
void DirectDrawDetourer::updateDirectDrawFunctions() {
	IDirectDraw * dd;
	DirectDrawCreate(NULL, &dd, NULL);
	void** vtable = (*(void***)(dd));

	DirectDrawDetourer::FUNCTIONS.CreateSurface = CREATE_SURFACE_CAST vtable[6];
	DirectDrawDetourer::FUNCTIONS.SetCooperativeLevel = SET_COOPERATIVE_LEVEL_CAST vtable[20];
	DirectDrawDetourer::FUNCTIONS.SetDisplayMode = SET_DISPLAY_MODE_CAST vtable[21];

	// Create a dummy surface
	dd->SetCooperativeLevel(NULL, DDSCL_NORMAL);
	IDirectDrawSurface* surface;
	DDSURFACEDESC desc = {};
	desc.dwSize = sizeof(DDSURFACEDESC);
	desc.dwFlags = DDSD_WIDTH | DDSD_HEIGHT | DDSD_CAPS;
	desc.ddsCaps.dwCaps = DDSCAPS_OFFSCREENPLAIN;
	desc.dwWidth = 100; desc.dwHeight = 100;
	dd->CreateSurface(&desc, &surface, NULL);

	vtable = (*(void***)(surface));
	DirectDrawDetourer::FUNCTIONS.SurfaceUnlock = SURFACE_UNLOCK_CAST vtable[32];
	DirectDrawDetourer::FUNCTIONS.SurfaceBlt = SURFACE_BLT_CAST vtable[5];
	DirectDrawDetourer::FUNCTIONS.SurfaceRelease = SURFACE_RELEASE_CAST vtable[2];

	surface->Release();
	dd->Release();
}

DirectDrawDetourer::~DirectDrawDetourer() {
	removeDetours();
	Detour::ResetData();
}

#define DETOUR_ATTACH(x) DetourAttach(&(PVOID&)DirectDrawDetourer::FUNCTIONS.x, Detour::x)
void DirectDrawDetourer::installDetours()  {
	DetourTransactionBegin();
	DetourUpdateThread(GetCurrentThread());
	DETOUR_ATTACH(CreateSurface);
	DETOUR_ATTACH(SetCooperativeLevel);
	DETOUR_ATTACH(SetDisplayMode);
	DETOUR_ATTACH(SurfaceUnlock);
	DETOUR_ATTACH(SurfaceBlt);
	DETOUR_ATTACH(SurfaceRelease);
	DetourTransactionCommit();
}

#define DETOUR_DETACH(x) DetourDetach(&(PVOID&)DirectDrawDetourer::FUNCTIONS.x, Detour::x)
void DirectDrawDetourer::removeDetours()  {
	DetourTransactionBegin();
	DetourUpdateThread(GetCurrentThread());
	DETOUR_DETACH(CreateSurface);
	DETOUR_DETACH(SetCooperativeLevel);
	DETOUR_DETACH(SetDisplayMode);
	DETOUR_DETACH(SurfaceUnlock);
	DETOUR_DETACH(SurfaceBlt);
	DETOUR_DETACH(SurfaceRelease);
	DetourTransactionCommit();
}