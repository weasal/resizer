// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#include <windows.h>
#include <wrl.h>
#include <shellapi.h>

#include <map>
#include <vector>
#include <string>
#include <exception>
#include <memory>

// USE_CUSTOM_CHAT
#include <deque>
#include <regex>			// for parsing lines in the chat log
#include <chrono>			// for timestamps in ChatMessage
#include <ctime>			// localtime
#include <iomanip>			// put_time
#include <sstream>
#include <locale>
#include <dwrite.h>
#include <cctype>			// tolower

#include <ddraw.h>
#include <d3d11_2.h>
#include <d2d1_2.h>

#include <initguid.h> 
#include <d2d1effects.h>
#include <d2d1effectauthor.h>  
#include <d2d1effecthelpers.h>

#include <psapi.h> // for memory

#ifdef _DEBUG
#include "debug.h"
#include <algorithm>
#endif

#include "detours.h"
