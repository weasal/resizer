#include "stdafx.h"
#include "globals.h"
#include "clearblackeffect.h"

#ifdef USE_RADAR_OPACITY
#include "radaropacity.h"
#endif

#define PRECT_TO_D2D1_RECT_F(rect) D2D1::RectF(static_cast<float>(rect->left), static_cast<float>(rect->top), static_cast<float>(rect->right), static_cast<float>(rect->bottom))
#ifdef _DEBUG
std::vector<D2D1_COLOR_F> colors;
#endif

void DirectXDrawer::Blank() {
	/*// This is the first thing continuum does each frame
	d2dContext->BeginDraw();
	drawState = DrawState::STARTED;*/

	VerifyDrawState();

	d2dContext->SetTarget(gameBitmap.Get());
	d2dContext->Clear(D2D1::ColorF(D2D1::ColorF::Black));
}

void DirectXDrawer::Present() {
	d2dContext->SetTarget(targetBitmap.Get());
	auto bufferSize = gameBitmap->GetSize();
	d2dContext->DrawBitmap(gameBitmap.Get(), nullptr, 1.f, D2D1_INTERPOLATION_MODE_CUBIC, &D2D1::RectF(0.f, 0.f, bufferSize.width, bufferSize.height), &D2D1::Matrix4x4F::Scale(scaleFactor, scaleFactor, 1.f));

#ifdef _DEBUG
	int count = 0;
	for (auto color : colors) {
		count++;
		RECT square = {};
		square.left = 15 * count;
		square.top = 0;
		square.right = square.left + 15;
		square.bottom = 15;
		ColorFill(color, &square);
	}
#endif

#ifdef USE_CUSTOM_CHAT
	d2dContext->SetTarget(targetBitmap.Get());
	D2D1_SIZE_F targetSize = targetBitmap->GetSize();
	float composedHeight = DrawComposedMessage();

	D2D1_RECT_F sourceRect = D2D1::RectF(0, 0, chatSize.width, chatSize.height);
	if (!chatcomposer->isMenuVisible()) {
		sourceRect.top = sourceRect.bottom - lineHeight * 5; // 5 = NUM_LINES
	}
	else {
		solidColorBrush->SetColor(D2D1::ColorF(D2D1::ColorF::Black, 0.9f));
		D2D1_RECT_F fillRect = D2D1::RectF(0.f, static_cast<float>(targetSize.height - chatSize.height),
			static_cast<float>(chatSize.width), static_cast<float>(targetSize.height - composedHeight));
		d2dContext->FillRectangle(fillRect, solidColorBrush.Get());
	}

	float destTop = targetSize.height - (sourceRect.bottom - sourceRect.top) - composedHeight;
	D2D1_RECT_F destRect = D2D1::RectF(0.f, destTop,
		static_cast<float>(chatSize.width), static_cast<float>(targetSize.height - composedHeight));

	d2dContext->DrawBitmap(chatBitmap.Get(), destRect, 1.f, D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR, sourceRect);
#endif

	d2dContext->EndDraw();
	drawState = DrawState::ENDED;

	DXGI_PRESENT_PARAMETERS presentDesc = {};
	HRESULT result = swapChain->Present1(0, 0, &presentDesc);

	WaitForSingleObjectEx(frameLatencyWaitableObject, 1000, true);

#ifdef _DEBUG
	if (FAILED(result))
		printf("Swapchain present failed! %i\n", result);
#endif
}

bool DirectXDrawer::SurfaceBitmapExists(int key) {
	return bitmaps.count(key) == 1;
}

void DirectXDrawer::CreateEmptyBitmap(int key, int width, int height) {
	ComPtr<ID2D1Bitmap1> bitmap;
	d2dContext->CreateBitmap(D2D1::SizeU(width, height),
		nullptr,
		0,
		bitmapProps,
		&bitmap);

	bitmaps[key] = bitmap;
}

void DirectXDrawer::CreateBitmapFromSurface(int key, int width, int height, void* data, int pitch) {
	if (SurfaceBitmapExists(key)) {
		d2dContext->Flush(); // radar is empty without this flush
		bitmaps.at(key)->CopyFromMemory(&D2D1::RectU(0, 0, width, height), data, pitch);
	} else { 
		ComPtr<ID2D1Bitmap1> bitmap;
		d2dContext->CreateBitmap(D2D1::SizeU(width, height),
			data,
			pitch,
			bitmapProps,
			&bitmap);

		bitmaps[key] = bitmap;
	}

	ClearBlackInBitmap(key);
}

void DirectXDrawer::DiscardBitmapForSurface(int key) {
	bitmaps.erase(key);
}

void DirectXDrawer::ClearBitmap(int key) {
	VerifyDrawState();

	d2dContext->SetTarget(bitmaps.at(key).Get());
	d2dContext->Clear(CLEAR_COLOR);
}

void DirectXDrawer::ClearBlackInBitmap(int key) {
	VerifyDrawState();

	ComPtr<ID2D1Bitmap1> bitmap = bitmaps.at(key);
	ComPtr<ID2D1Effect> clearBlackEffect;
	d2dContext->CreateEffect(CLSID_BlackClearEffect, &clearBlackEffect);
	clearBlackEffect->SetInput(0, bitmap.Get());

	D2D1_SIZE_U size = bitmap->GetPixelSize();
	ComPtr<ID2D1Bitmap1> outputBitmap;
	d2dContext->CreateBitmap(size, nullptr, 0, bitmapProps, &outputBitmap);
	d2dContext->SetTarget(outputBitmap.Get());
	d2dContext->Clear(CLEAR_COLOR);
	d2dContext->DrawImage(clearBlackEffect.Get());
	bitmaps[key] = outputBitmap;
}

void DirectXDrawer::ColorFill(D2D1_COLOR_F color, const RECT* rect) {
	VerifyDrawState();

#ifdef _DEBUG
	if (std::find_if(colors.begin(), colors.end(), [&color](D2D1_COLOR_F& c) {
		if (c.r != color.r || c.g != color.g || c.b != color.b)
			return false;

		return true;
	}) == colors.end()) {
		colors.push_back(color);
		DWORD rgb = (static_cast<int>(255 * color.r) << 16) + (static_cast<int>(255 * color.g) << 8) + static_cast<int>(255 * color.b);
		rgb |= 0xFF000000;
		printf("Colorfilled the screen new with color #%i: (%f, %f, %f), approx %x \n", colors.size(), color.r, color.g, color.b, rgb);
	}
#endif

	d2dContext->SetTarget(gameBitmap.Get());

	if (rect == nullptr) { // Null parameter indicates to colorfill entire surface
		d2dContext->Clear(color);
	}
	else {
		solidColorBrush->SetColor(color);
		d2dContext->FillRectangle(PRECT_TO_D2D1_RECT_F(rect), solidColorBrush.Get());
	}
}

void DirectXDrawer::ColorFillBitmap(int key, D2D1_COLOR_F color, const RECT* rect) {
	VerifyDrawState();

	ComPtr<ID2D1Bitmap1> bitmap = bitmaps.at(key);

#ifdef _DEBUG
	if (std::find_if(colors.begin(), colors.end(), [&color](D2D1_COLOR_F& c) {
		if (c.r != color.r || c.g != color.g || c.b != color.b)
			return false;

		return true;
	}) == colors.end()) {
		colors.push_back(color);
		printf("Colorfilled a bitmap with new color #%i: (%f, %f, %f)\n", colors.size(), color.r, color.g, color.b);
	}
#endif

	d2dContext->SetTarget(bitmap.Get());
	if (rect == nullptr) {
		d2dContext->Clear(color);
	}
	else {
		solidColorBrush->SetColor(color);
		d2dContext->FillRectangle(PRECT_TO_D2D1_RECT_F(rect), solidColorBrush.Get());
	}
}

// TODO: Optimize, 90% of the calls in a frame are to this method
void DirectXDrawer::DrawBitmap(int key, const RECT* sourceRect, const RECT* destRect) {
	VerifyDrawState();

	D2D1_RECT_F* sourceRectPtr = NULL;
	D2D1_RECT_F* destRectPtr = NULL;

	if (sourceRect)
		sourceRectPtr = &PRECT_TO_D2D1_RECT_F(sourceRect);

	if (destRect)
		destRectPtr = &PRECT_TO_D2D1_RECT_F(destRect);

	d2dContext->SetTarget(gameBitmap.Get());

#ifdef USE_RADAR_OPACITY
	// check if painting the radar bitmap by seeing if the destination rect matches
	// its paint location
	if (isRadarPaintDestination(*destRect)) { 
		// before painting the bitmap, paint a transparent background
		solidColorBrush->SetColor(RADAR_BACKGROUND_COLOR);
		d2dContext->FillRectangle(PRECT_TO_D2D1_RECT_F(destRect), solidColorBrush.Get());
	}
#endif

	d2dContext->DrawBitmap(bitmaps.at(key).Get(), destRectPtr, 1.f, D2D1_BITMAP_INTERPOLATION_MODE_LINEAR, sourceRectPtr);
}

void DirectXDrawer::DrawBitmapToBitmap(int sourcekey, const RECT* sourceRect, int destkey, const RECT* destRect) {
	VerifyDrawState();

	D2D1_RECT_F* sourceRectPtr = NULL;
	D2D1_RECT_F* destRectPtr = NULL;

	if (sourceRect)
		sourceRectPtr = &PRECT_TO_D2D1_RECT_F(sourceRect);

	if (destRect)
		destRectPtr = &PRECT_TO_D2D1_RECT_F(destRect);

	d2dContext->SetTarget(bitmaps.at(destkey).Get());
	d2dContext->DrawBitmap(bitmaps.at(sourcekey).Get(), destRectPtr, 1.f, D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR, sourceRectPtr);
}


#ifdef USE_CUSTOM_CHAT
inline std::wstring stringToWstring(const std::string& str) {
	return std::wstring_convert<std::codecvt<wchar_t, char, mbstate_t>>().from_bytes(str);
}

D2D1_COLOR_F getChatColorFromType(chat::Message::Type type) {
	D2D1_COLOR_F color;

	switch (type) {
	case chat::Message::Type::Team:
		color = D2D1::ColorF(0xEFB525, 1.f);
		break;
	case chat::Message::Type::Private:
		color = D2D1::ColorF(0x9FD788, 1.f);
		break;
	case chat::Message::Type::Channel:
		color = D2D1::ColorF(0xAC3D39, 1.f);
		break;
	case chat::Message::Type::Public:
		color = D2D1::ColorF(0x4BBDC3, 1.f);
		break;
	default:
		color = D2D1::ColorF(0x999999, 1.f);
		break;
	}

	return color;
}

std::string createMessageText(chat::Message message) {
	std::stringstream output;

	// add a timestamp
	auto in_time_t = std::chrono::system_clock::to_time_t(message.receivedAt);
	output << std::put_time(std::localtime(&in_time_t), "%M:%S ");

	// add the sender
	if (!message.sender.empty())
		output << message.sender << "> ";

	// add the content
	output << message.content;
	return output.str();
}

void DirectXDrawer::DrawNewMessage(chat::Message message) { // DrawNewMessage(chat::Message message)
	VerifyDrawState();
	
	std::wstring text = stringToWstring(createMessageText(message));

	ComPtr<IDWriteTextLayout> layout;
	dwriteFactory->CreateTextLayout(text.c_str(), text.size(), textFormat.Get(), chatSize.width, chatSize.height, &layout);
	
	// bold the layout in the name
	DWRITE_TEXT_RANGE textRange = {};
	if (!message.sender.empty()) {
		textRange.startPosition = 6; // "%M:%S " is 6 characters
		textRange.length = message.sender.size();
		layout->SetFontWeight(DWRITE_FONT_WEIGHT_BOLD, textRange);
	}

	// get the height of the new message
	DWRITE_TEXT_METRICS metrics = {};
	layout->GetMetrics(&metrics);

	// translate the previous bitmap up by the height using an affine transform effect
	ComPtr<ID2D1Effect> affineTransformEffect;
	d2dContext->CreateEffect(CLSID_D2D12DAffineTransform, &affineTransformEffect);
	affineTransformEffect->SetInput(0, chatBitmap.Get());
	D2D1_MATRIX_3X2_F matrix = D2D1::Matrix3x2F::Translation(D2D1::SizeF(0, -metrics.height));
	affineTransformEffect->SetValue(D2D1_2DAFFINETRANSFORM_PROP_TRANSFORM_MATRIX, matrix);
	affineTransformEffect->SetValue(D2D1_2DAFFINETRANSFORM_PROP_INTERPOLATION_MODE,
		D2D1_2DAFFINETRANSFORM_INTERPOLATION_MODE::D2D1_2DAFFINETRANSFORM_INTERPOLATION_MODE_NEAREST_NEIGHBOR);

	d2dContext->SetTarget(chatBitmapCopy.Get());
	d2dContext->Clear(CLEAR_COLOR);
	d2dContext->DrawImage(affineTransformEffect.Get());

	ComPtr<ID2D1Bitmap1> temp = chatBitmap;
	chatBitmap = chatBitmapCopy;
	chatBitmapCopy = temp;

	// draw a slightly dark background
	float drawY = chatSize.height - metrics.height;
	solidColorBrush->SetColor(D2D1::ColorF(D2D1::ColorF::Black, 0.4f));
	d2dContext->FillRectangle(D2D1::RectF(0, drawY, metrics.width + 2, drawY + metrics.height),
		solidColorBrush.Get());

	// draw the chat text
	solidColorBrush->SetColor(getChatColorFromType(message.type));
	d2dContext->DrawTextLayout(D2D1::Point2F(0, drawY), layout.Get(), solidColorBrush.Get());
}

float DirectXDrawer::DrawComposedMessage() {
	chat::Message composedMessage = chatcomposer->getMessage();
	if (!composedMessage.content.empty()) {
		ComPtr<IDWriteTextLayout> layout;
		std::wstring text = stringToWstring(composedMessage.content);
		dwriteFactory->CreateTextLayout(text.c_str(), text.size(), textFormat.Get(), chatSize.width, chatSize.height, &layout);

		DWRITE_TEXT_METRICS metrics = {};
		layout->GetMetrics(&metrics);

		d2dContext->SetTarget(targetBitmap.Get());

		// draw a slightly dark background
		float windowHeight = targetBitmap->GetSize().height;
		float drawY = windowHeight - metrics.height;
		solidColorBrush->SetColor(D2D1::ColorF(D2D1::ColorF::Black, 0.4f));
		d2dContext->FillRectangle(D2D1::RectF(0.f, drawY, 0.f + metrics.width + 2.f, windowHeight),
			solidColorBrush.Get());

		// draw text
		solidColorBrush->SetColor(getChatColorFromType(composedMessage.type));
		d2dContext->DrawTextLayout(D2D1::Point2F(0, drawY), layout.Get(), solidColorBrush.Get());
		return metrics.height;
	}

	return 0;
}
#endif