#include "stdafx.h"

#ifdef _DEBUG
void CreateConsole() {
	 AllocConsole();
	 AttachConsole(GetCurrentProcessId());

	 long stdOutputHandle = (long)GetStdHandle(STD_OUTPUT_HANDLE);
	 int outFileDesc = _open_osfhandle(stdOutputHandle, _O_TEXT);
	 FILE * fp = _fdopen(outFileDesc, "w");
	 *stdout = *fp;
	 setvbuf(stdout, NULL, _IONBF, 0);
	 std::ios::sync_with_stdio();
}
#endif