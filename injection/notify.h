#pragma once
#include <string>
void InitToasts();
void CreatePrivateMessageToast(std::string sender, std::string message);
void CreatePrivateMessageToastW(std::wstring sender, std::wstring message);