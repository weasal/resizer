#include "stdafx.h"
#include "windowutil.h"

CONTINUUM_WINDOW_TYPE getWindowType(HWND window) {
	 CONTINUUM_WINDOW_TYPE type = CONTINUUM_WINDOW_TYPE::UNKNOWN_WINDOW;

	 wchar_t buffer[256];
	 if (GetWindowText(window, buffer, sizeof(buffer) / sizeof(wchar_t))) {
		 std::wstring windowName(buffer);

		 if (windowName == CONTINUUM_GAME_WINDOW_TITLE)
			 type = CONTINUUM_WINDOW_TYPE::GAME_WINDOW;
		 else if (windowName == CONTINUUM_LAUNCHER_WINDOW_TITLE)
			 type = CONTINUUM_WINDOW_TYPE::LAUNCHER_WINDOW;
		 else {
			 GetClassName(window, buffer, sizeof(buffer));
			 std::wstring windowClassName(buffer);
			 if (windowClassName == CONTINUUM_CHAT_WINDOW_CLASS_NAME)
				 type = CONTINUUM_WINDOW_TYPE::CHAT_WINDOW;
		 }
	 }

	 return type;
}

/**
 * Sets the last key in the Continuum launcher's window menu to be titled 
 * "[xres] x [yres] x32 (Custom)". Returns that entry's menu id.
 */
#define RESOLUTION_MENU_INDEX 3
unsigned int SetLastLauncherResolutionMenuEntry(HWND continuumUIWindow, int resX, int resY) {
	if (continuumUIWindow == NULL) {
		return NULL;
	}

	HMENU continuumMenu = GetMenu(continuumUIWindow);
	HMENU resolutionSubMenu = GetSubMenu(continuumMenu, RESOLUTION_MENU_INDEX);

	if (resolutionSubMenu) {
		int numResolutions = GetMenuItemCount(resolutionSubMenu);
		MENUITEMINFO mii = {0};
		mii.cbSize = sizeof(MENUITEMINFO);
		mii.fType = MFT_STRING;
		mii.fMask = MIIM_STATE | MIIM_STRING | MIIM_ID;
		GetMenuItemInfo(resolutionSubMenu, numResolutions - 3, MF_BYPOSITION, &mii);

		// create the menu entry title
		wchar_t entryTitle[64];
		wsprintf(entryTitle, TEXT("%d x %d x 32 (Custom)"), resX, resY);

		mii.dwTypeData = entryTitle;
		SetMenuItemInfo(resolutionSubMenu, numResolutions - 3, MF_BYPOSITION, &mii);
		return mii.wID;
	}
#ifdef _DEBUG
	else {
		printf("Unable to find resolution subMenu\n");
	}
#endif

	return NULL;
}

// Check if app has focus
bool ApplicationIsActive()
{
	HWND activeWindow = GetForegroundWindow();
	if (activeWindow == NULL) {
		return false;
	}

	DWORD procId;
	GetWindowThreadProcessId(activeWindow, &procId);
	return procId == GetCurrentProcessId();
}