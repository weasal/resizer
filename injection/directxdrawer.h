#include "features.h"
using namespace Microsoft::WRL;

/* TODO
[1] Use bitmap brushes? -- performance improvement, might have to discard certain bitmap brushes and 
	recreate them when continuum changes their contents (e.g. tilebmp changes on arena change)
[2] Render whole level to bitmap, use that as a bitmap brush. This is a replacement for continuum redrawing
	each visible tile individually every single frame. Drawbacks: animated tiles (asteroids), and gates wont
	work properly with this technique. Solution: for gates -- can draw over them with black when they're empty,
	for asteroids -- ignore animation (not ideal :/), or draw per frame
*/

class DirectXDrawer {
private:
	ComPtr<ID3D11Device> device;
	ComPtr<IDXGISwapChain2> swapChain;
	ComPtr<ID2D1Factory2> d2dFactory;
	ComPtr<ID2D1DeviceContext1> d2dContext;
	ComPtr<ID2D1Bitmap1> targetBitmap;
	ComPtr<ID2D1Bitmap1> gameBitmap; // draw to this, then paint (with scaling) onto targetBitmap
	HANDLE frameLatencyWaitableObject;

	HWND gameWindow;
	enum class DrawState { STARTED, ENDED } drawState;
	std::map<int, ComPtr<ID2D1Bitmap1>> bitmaps; //TODO: [1]
	float scaleFactor;

	void InitializeD3DDeviceAndContext();
	void InitializeDirect2D();
	void InitializeD2DTarget();
	void InitializeWindowDependentResources();
	void InitializeIndependentResources();
	void VerifyDrawState();

	const D2D1_COLOR_F CLEAR_COLOR = D2D1::ColorF(D2D1::ColorF::Black, 0.f);
	D2D1_MATRIX_3X2_F defaultTransform;
	ComPtr<ID2D1SolidColorBrush> solidColorBrush;
	D2D1_BITMAP_PROPERTIES1 bitmapProps;

#ifdef USE_CUSTOM_CHAT
	ComPtr<ID2D1Bitmap1> chatBitmap;
	ComPtr<ID2D1Bitmap1> chatBitmapCopy; // copy need for affine transform effect
	ComPtr<IDWriteFactory> dwriteFactory;
	ComPtr<IDWriteTextFormat> textFormat;
	D2D1_SIZE_F chatSize;
	float lineHeight;
#endif
public:
	static LRESULT CALLBACK WindowProcedure(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	DirectXDrawer(int xres, int yres);
	~DirectXDrawer();

	bool SurfaceBitmapExists(int key);
	void CreateEmptyBitmap(int key, int width, int height);
	void CreateBitmapFromSurface(int key, int width, int height, void* rawSurfaceData, int pitch);
	void DiscardBitmapForSurface(int key);

	void ClearBitmap(int key);
	void ClearBlackInBitmap(int key);
	void ColorFill(D2D1_COLOR_F color, const RECT* rect); // to screen
	void ColorFillBitmap(int destkey, D2D1_COLOR_F color, const RECT* rect);
	void DrawBitmap(int key, const RECT* sourceRect, const RECT* destRect); // onto screen
	void DrawBitmapToBitmap(int sourcekey, const RECT* sourceRect, int destkey, const RECT* destRect);

	void Blank();
	void Present();

#ifdef USE_CUSTOM_CHAT
	// void AddChatMessage(chat::Message message);
	// void DrawChat();

	void DrawNewMessage(chat::Message message);
	float DrawComposedMessage(); // return the height of the composed message
#endif
};