#include "stdafx.h"
#include "globals.h"
#include "notify.h"
#include "windowutil.h"

#ifdef USE_CUSTOM_CHAT
BOOL(WINAPI *OriginalWriteFile)(HANDLE, LPCVOID, DWORD, LPDWORD, LPOVERLAPPED) = WriteFile;
BOOL WINAPI WriteFileDetour(HANDLE handle, LPCVOID buffer, DWORD size, LPDWORD written, LPOVERLAPPED overlapped) {
	if (chatwatcher->isChatLogfile(handle)) {
		std::string line(reinterpret_cast<const char*>(buffer), size);
		line.resize(line.size() - 2); // get rid of the newline
		chatwatcher->handleLogLine(line);

#ifdef _DEBUG
		printf("Chat log write: %s\n", line.c_str());
#endif
	}

	return OriginalWriteFile(handle, buffer, size, written, overlapped);
}

chat::LogWatcher::LogWatcher() {
	DetourTransactionBegin();
	DetourUpdateThread(GetCurrentThread());
	DetourAttach(&(PVOID&)OriginalWriteFile, WriteFileDetour);
	DetourTransactionCommit();

	InitToasts();
}

chat::LogWatcher::~LogWatcher() {
	DetourTransactionBegin();
	DetourUpdateThread(GetCurrentThread());
	DetourDetach(&(PVOID&)OriginalWriteFile, WriteFileDetour);
	DetourTransactionCommit();
}

bool chat::LogWatcher::isChatLogfile(HANDLE handle) {
	if (handle == logfileHandle)
		return true;

	if (handle != INVALID_HANDLE_VALUE) {
		TCHAR buffer[MAX_PATH];
		GetFinalPathNameByHandle(handle, buffer, MAX_PATH, NULL);

#ifdef _DEBUG
		if (handle != GetStdHandle(STD_OUTPUT_HANDLE)) {
			wprintf(L"isChatLogfile: %s\n", buffer);
		}
#endif

		std::basic_string<TCHAR> path(buffer);
		if (path.find(TEXT(".log")) != std::basic_string<TCHAR>::npos) {
#ifdef _DEBUG
			printf("Found logfile\n");
#endif
			logfileHandle = handle;
			return true;
		}
	}

	return false;
}

#define MESSAGE_HISTORY_LIMIT 100
const std::regex chat::LogWatcher::ChatRegex = std::regex(R"::(^(P|T|C|E|\s)\s(?::|([0-9]+:|\s*))(?::(.+?):|(.+?)>)\s*(.*)$)::");
const std::regex chat::LogWatcher::KillRegex = std::regex(R"::(^\s+(.+)\(([0-9]+)\) killed by: (.+)$)::");
void chat::LogWatcher::handleLogLine(std::string line) {
	Message msg = {};
	msg.receivedAt = std::chrono::system_clock::now();

	std::smatch match;
	if (std::regex_match(line, match, ChatRegex)) {
		std::string typestr = match[1].str();
		std::string player = match[3].str() + match[4].str();
		std::string message = match[5].str();

		switch (typestr.at(0)) {
		case 'P':
			msg.type = Message::Type::Private;
			if (!ApplicationIsActive()) {
				CreatePrivateMessageToast(player, message);
			}
			break;
		case 'T':
			msg.type = Message::Type::Team;
			break;
		case 'C':
			msg.type = Message::Type::Channel;
			break;
		case 'E':
			msg.type = Message::Type::Freq;
			break;
		default:
			msg.type = Message::Type::Public;
		}

		msg.sender = player;

		if (msg.type == Message::Type::Channel) {
			std::stringstream channelmsg;
			channelmsg << "[" << std::atoi(match[2].str().c_str()) << "] " << message;
			msg.content = channelmsg.str();
		} else {
			msg.content = message;
		}
	} else {
		msg.type = Message::Type::Other;
		msg.content = line;
	}

	if (msg.type == Message::Type::Public && msg.content[0] == '?') {
		printf("Got a command: %s\n", msg.content.c_str());
		std::string command = msg.content.substr(1);
		handleCommand(command);
	}

	messages.push_back(msg);
	if (messages.size() > MESSAGE_HISTORY_LIMIT)
		messages.pop_front();

	if (drawer)
		drawer->DrawNewMessage(msg);
}

void chat::LogWatcher::handleCommand(std::string command) {
	if (command == "openlink") {
		// find the most recent link and open it
		std::regex urlRegex(R"::(((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[.\!\/\\w]*))?))::", std::regex::icase);
		for (auto iter = messages.rbegin(); iter != messages.rend(); iter++) {
			std::string content = iter->content;
			std::smatch match;
			if (std::regex_match(content, match, urlRegex)) {
				ShellExecuteA(NULL, "open", match[0].str().c_str(), NULL, NULL, SW_SHOWMAXIMIZED);
				break;
			}
		}
	}
}

std::vector<chat::Message> chat::LogWatcher::getRecentMessages(unsigned int count) {
	if (count > messages.size())
		count = messages.size();

	std::vector<Message> recent;
	for (auto iter = messages.rbegin(); count != 0; iter++) {
		recent.push_back(*iter);
		count--;
	}

	return recent;
}
#endif // USE_CUSTOM_CHAT