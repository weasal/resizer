#include "stdafx.h"
#include "globals.h"

#ifdef USE_RADAR_OPACITY
// these macros take in the width of the game surface
#define MAP_SIZE_FROM_WIDTH(x)		floor(x / 1.25f)			// size of the map (i.e. the thing shown when holding alt)	
#define ARENA_WIDTH_IN_PIXELS		1024 * 16					// arenas consist of 1024 tiles that are 16 pixels wide and tall
#define RADAR_SCALE_FROM_WIDTH(x)	ARENA_WIDTH_IN_PIXELS / MAP_SIZE_FROM_WIDTH(x)
#define RADAR_SIZE_FROM_WIDTH(x)	((x / 6) - ((x / 6) % 4))	// x must be integer (for integer div)
#define RADAR_PAINT_OFFSET			6							// offset from the bottom and right edge of screen
bool isRadarPaintDestination(const RECT& dest) {
	int radarSize = RADAR_SIZE_FROM_WIDTH(Detour::Data.gameWidth);

	if (dest.right - dest.left == radarSize &&
		dest.right == Detour::Data.gameWidth - RADAR_PAINT_OFFSET &&
		dest.bottom == Detour::Data.gameHeight - RADAR_PAINT_OFFSET)
		return true;

	return false;
}
#endif