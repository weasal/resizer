#define CONTINUUM_LAUNCHER_WINDOW_TITLE L"Continuum 0.40"
#define CONTINUUM_GAME_WINDOW_TITLE L"Continuum"
#define CONTINUUM_CHAT_WINDOW_CLASS_NAME L"ChatClass"

enum class CONTINUUM_WINDOW_TYPE {
	LAUNCHER_WINDOW, GAME_WINDOW, CHAT_WINDOW, UNKNOWN_WINDOW
};

CONTINUUM_WINDOW_TYPE getWindowType(HWND window);
unsigned int SetLastLauncherResolutionMenuEntry(HWND continuumUIWindow, int resX, int resY);
bool ApplicationIsActive();

inline HWND getContinuumGameWindow() {
	return FindWindow(NULL, CONTINUUM_GAME_WINDOW_TITLE);
}
