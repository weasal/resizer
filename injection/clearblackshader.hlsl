Texture2D InputTexture : register(t0);
SamplerState InputSampler : register(s0);

float4 main(
	float4 clipSpaceOutput  : SV_POSITION,
	float4 sceneSpaceOutput : SCENE_POSITION,
	float4 texelSpaceInput0 : TEXCOORD0
	) : SV_Target
{
	float2 sampleLocation =
	texelSpaceInput0.xy    // Sample position for the current output pixel.
	+ float2(0, 0)        // An offset from which to sample the input, specified in pixels.
	* texelSpaceInput0.zw; // Multiplier that converts pixel offset to sample position offset.

	float4 color = InputTexture.Sample(
		InputSampler,          // Sampler and Texture must match for a given input.
		sampleLocation
		);

	color.a = 1.f;

	if (color.r == 0.f && color.g == 0.f && color.b == 0.f)
		return float4(0.f, 0.f, 0.f, 0.f);
	else
		return color;
}
