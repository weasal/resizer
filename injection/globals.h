#include "features.h"
#include "chat.h"
#include "directdrawdetourer.h"
#include "directxdrawer.h"

#ifdef CREATE_GLOBALS
#define GLOBAL(x, y) x = y;
#else
#define GLOBAL(x, y) extern x;
#endif

GLOBAL(bool initialized, false)
GLOBAL(HINSTANCE hinst, NULL)
GLOBAL(DirectDrawDetourer* detour, nullptr)
GLOBAL(DirectXDrawer* drawer, nullptr)

#ifdef USE_CUSTOM_CHAT
GLOBAL(chat::LogWatcher* chatwatcher, nullptr)
GLOBAL(chat::Composer* chatcomposer, nullptr)
#endif