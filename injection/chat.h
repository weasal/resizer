#ifdef USE_CUSTOM_CHAT
#include "continuummemory.h"

/* TODO
[1] Enhancements? 
	- With replies to pms, also include who they're sent to (e.g. "(reply to <name>)">)
	- Reorder chat channel indicator (instead of "<name>> [<channel>]", do "[<channel>] <name>>")
	- Custom resizer commands
		ideas: ?openlink -- opens the most recent link in a web browser
	- When someone mentions a user defined string play a beep (user sets what strings to play beeps to,
		could be their name, their nickname that they're commonly addressed by, or anything)
	- Text macros/expansion?? (synthesized input)
*/

namespace chat {
	struct Message {
		enum class Type { Public, Private, Team, Channel, Freq, Other };
		Type type;
		std::string sender;
		std::string raw;
		std::string content;
		std::string receiver; // used with private messages TODO: implement (private message sent to: <recipient>)
		std::chrono::system_clock::time_point receivedAt;
	};

	class Composer {
	private:
		bool menuVisible;
		unsigned int messagerIndex; // used with pms, changes when : is typed
		Message current;
		ContinuumMemory memory;
		static const std::regex PrivateMessageRegex;
	public:
		Composer();
		~Composer();
		void handleInput(char c);
		Message getMessage();
		bool isMenuVisible();
	};

	class LogWatcher {
	private:
		std::deque<Message> messages;
		HANDLE logfileHandle;

		static const std::regex ChatRegex;
		static const std::regex KillRegex;
	public:
		LogWatcher();
		~LogWatcher();

		bool isChatLogfile(HANDLE handle);
		void handleLogLine(std::string line);
		std::vector<Message> getRecentMessages(unsigned int count);
		void handleCommand(std::string command);
	};
}
#endif

