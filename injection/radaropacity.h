#include "features.h"

#ifdef USE_RADAR_OPACITY
#define RADAR_BACKGROUND_COLOR			D2D1::ColorF(0xFF153D14, 0.625f)
bool isRadarPaintDestination(const RECT& dest);
#endif