#include "stdafx.h"
#include "windowutil.h"
#include <winrt/Windows.Data.Xml.Dom.h>
#include <winrt/Windows.UI.Notifications.h>
using namespace winrt;
using namespace winrt::Windows::Data::Xml::Dom;
using namespace winrt::Windows::UI::Notifications;
using winrt::Windows::Foundation::TypedEventHandler;

// should map to whatever app id is set on the shortcut from the main app
#define RESIZER_APP_ID TEXT("Weasalss.Continuum.Resizer")

void InitToasts() {
	init_apartment();
}

void CreatePrivateMessageToastW(std::wstring sender, std::wstring message) {
	XmlDocument doc;
	doc.LoadXml(
		TEXT(R"xml(<toast launch="app-defined-string">
			<visual>
				<binding template="ToastGeneric">
					<text hint-maxLines="1">Crescendo</text>
					<text >sending you a PM LOL</text>
				</binding>
			</visual>
	
			<audio src="ms-winsoundevent:Notification.IM"/>
		</toast>)xml"));

	// GetElementById isn't working...
	auto senderText = doc.GetElementsByTagName(TEXT("text")).Item(0).as<XmlElement>();
	auto messageText = doc.GetElementsByTagName(TEXT("text")).Item(1).as<XmlElement>();
	senderText.InnerText(sender);
	messageText.InnerText(message);

	ToastNotification toast(doc);

	// Set the click handler to bring focus to continuum
	/* Not working for some reason... 
	TypedEventHandler<ToastNotification, winrt::Windows::Foundation::IInspectable> toastHandler([](ToastNotification sender, winrt::Windows::Foundation::IInspectable object) {
		ToastActivatedEventArgs args = object.as<ToastActivatedEventArgs>();

		// Clicking the toast will activate the main continuum window
		SetFocus(getContinuumGameWindow());
	}); 
	toast.Activated(toastHandler);*/

	ToastNotificationManager::CreateToastNotifier(RESIZER_APP_ID).Show(toast);
}

void CreatePrivateMessageToast(std::string sender, std::string message) {
	std::wstring wplayer(sender.size(), L' ');
	wplayer.resize(std::mbstowcs(&wplayer[0], sender.c_str(), sender.size()));
	std::wstring wmessage(message.size(), L' ');
	wmessage.resize(std::mbstowcs(&wmessage[0], message.c_str(), std::clamp<int>(message.size(), 0, 140)));
	CreatePrivateMessageToastW(wplayer, wmessage);
}


