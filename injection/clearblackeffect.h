DEFINE_GUID(CLSID_BlackClearEffect, 0x71560959, 0xe4f5, 0x43dd, 0x95, 0x7d, 0x72, 0x33, 0x17, 0xe0, 0x50, 0xad);

// Our effect contains one transform, which is simply a wrapper around a pixel shader. As such,
// we can simply make the effect itself act as the transform.
class ClearBlackEffect : public ID2D1EffectImpl, public ID2D1DrawTransform
{
public:
	// Declare effect registration methods.
	static HRESULT Register(_In_ ID2D1Factory1* pFactory);

	static HRESULT __stdcall CreateClearBlackImpl(_Outptr_ IUnknown** ppEffectImpl);

	// Declare ID2D1EffectImpl implementation methods.
	IFACEMETHODIMP Initialize(
		_In_ ID2D1EffectContext* pContextInternal,
		_In_ ID2D1TransformGraph* pTransformGraph
		);

	IFACEMETHODIMP PrepareForRender(D2D1_CHANGE_TYPE changeType);

	IFACEMETHODIMP SetGraph(_In_ ID2D1TransformGraph* pGraph);

	// Declare ID2D1DrawTransform implementation methods.
	IFACEMETHODIMP SetDrawInfo(_In_ ID2D1DrawInfo* pRenderInfo);

	// Declare ID2D1Transform implementation methods.
	IFACEMETHODIMP MapOutputRectToInputRects(
		_In_ const D2D1_RECT_L* pOutputRect,
		_Out_writes_(inputRectCount) D2D1_RECT_L* pInputRects,
		UINT32 inputRectCount
		) const;

	IFACEMETHODIMP MapInputRectsToOutputRect(
		_In_reads_(inputRectCount) CONST D2D1_RECT_L* pInputRects,
		_In_reads_(inputRectCount) CONST D2D1_RECT_L* pInputOpaqueSubRects,
		UINT32 inputRectCount,
		_Out_ D2D1_RECT_L* pOutputRect,
		_Out_ D2D1_RECT_L* pOutputOpaqueSubRect
		);

	IFACEMETHODIMP MapInvalidRect(
		UINT32 inputIndex,
		D2D1_RECT_L invalidInputRect,
		_Out_ D2D1_RECT_L* pInvalidOutputRect
		) const;

	// Declare ID2D1TransformNode implementation methods.
	IFACEMETHODIMP_(UINT32) GetInputCount() const;

	// Declare IUnknown implementation methods.
	IFACEMETHODIMP_(ULONG) AddRef();
	IFACEMETHODIMP_(ULONG) Release();
	IFACEMETHODIMP QueryInterface(_In_ REFIID riid, _Outptr_ void** ppOutput);

private:
	ClearBlackEffect();

	Microsoft::WRL::ComPtr<ID2D1DrawInfo>      m_drawInfo;
	Microsoft::WRL::ComPtr<ID2D1EffectContext> m_effectContext;
	LONG                                       m_refCount;
};
