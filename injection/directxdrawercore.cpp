#include "stdafx.h"
#include "clearblackeffect.h"
#include "globals.h"
#include "windowutil.h"

#define DIRECTX_WINDOW_CLASS_NAME	L"Continuum (DirectX) Controller"
#define DIRECTX_WINDOW_TITLE		L"Continuum (DirectX) Controller"

DirectXDrawer::DirectXDrawer(int xres, int yres) :
	drawState(DrawState::ENDED), 
	frameLatencyWaitableObject(), 
	defaultTransform{},
	bitmapProps(D2D1::BitmapProperties1(D2D1_BITMAP_OPTIONS_TARGET,
				D2D1::PixelFormat(DXGI_FORMAT_B8G8R8A8_UNORM, D2D1_ALPHA_MODE_PREMULTIPLIED))),
	scaleFactor(1.f)
{
	gameWindow = getContinuumGameWindow();

	try {
		InitializeD3DDeviceAndContext();
		InitializeWindowDependentResources();
		InitializeDirect2D();
		InitializeD2DTarget();
		InitializeIndependentResources();

		// Resizing functionality:
		// We create a (possibly) higher resolution bitmap where the bigger res game is drawn to
		// then we squish that by drawing to the targetBitmap (which is the desktop res size) with interpolation
		d2dContext->CreateBitmap(D2D1::SizeU(xres, yres), nullptr, 0, bitmapProps, &gameBitmap);

		// Determine how much to scale (assume identical scaling in x and y directions)
		scaleFactor = targetBitmap.Get()->GetSize().width / gameBitmap.Get()->GetSize().width;
	}
	catch (const std::exception& e) {
		printf("DirectX error: %s\n", e.what());
	}
}

DirectXDrawer::~DirectXDrawer() {
	// avoid D2D DEBUG ERROR - A device context was destroyed while inside a BeginDraw/EndDraw block.
	if (drawState != DrawState::ENDED)
		d2dContext->EndDraw();
}

void DirectXDrawer::VerifyDrawState() {
#ifdef _DEBUG
	// TODO: use this instead (performance)
	/*if (drawState == DrawState::ENDED) {
		throw std::exception("Trying to draw something without BeginDraw");
	}*/
#endif

	if (drawState == DrawState::ENDED) {
		d2dContext->BeginDraw();
		drawState = DrawState::STARTED;
	}
}

void DirectXDrawer::InitializeD3DDeviceAndContext() {
	// This flag is required in order to enable compatibility with Direct2D.
	UINT createDeviceFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;

#ifdef _DEBUG
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	// This array defines the ordering of feature levels that D3D should attempt to create.
	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
		D3D_FEATURE_LEVEL_9_3,
		D3D_FEATURE_LEVEL_9_1
	};

	ComPtr<ID3D11DeviceContext> dc;

	HRESULT result = D3D11CreateDevice(
		nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, createDeviceFlags,
		featureLevels, ARRAYSIZE(featureLevels), D3D11_SDK_VERSION, &device, nullptr, &dc);

	if (FAILED(result))
		throw std::exception("D3D11CreateDevice failed");
}

void DirectXDrawer::InitializeWindowDependentResources() {
	if (gameWindow == NULL)
		throw std::exception("Initializing window dependent resources with NULL HWND");

	HRESULT result = S_OK;

	ComPtr<IDXGIDevice2> dxgiDevice;
	ComPtr<IDXGIFactory2> dxgiFactory;
	result = device.As(&dxgiDevice);
	if (SUCCEEDED(result)) {
		ComPtr<IDXGIAdapter> adapter;
		if (SUCCEEDED(dxgiDevice->GetAdapter(&adapter))) {
			result = adapter->GetParent(__uuidof(IDXGIFactory2), &dxgiFactory);
		}

#if defined(_DEBUG) && defined(LIST_MODES)
		ComPtr<IDXGIOutput> dxgiOutput;
		adapter->EnumOutputs(0, &dxgiOutput);

		UINT numModes = 0;
		dxgiOutput->GetDisplayModeList(DXGI_FORMAT_B8G8R8A8_UNORM, 0, &numModes, NULL);
		DXGI_MODE_DESC* modes = new DXGI_MODE_DESC[numModes];
		dxgiOutput->GetDisplayModeList(DXGI_FORMAT_B8G8R8A8_UNORM, 0, &numModes, modes);

		for (unsigned int i = 0; i < numModes; i++)
			printf("Mode: %i/%i\n", modes[i].RefreshRate.Numerator, modes[i].RefreshRate.Denominator);

		delete modes;
#endif
	}

	if (FAILED(result))
		throw std::exception("Failed to obtain IDXGIFactory2");

	DXGI_SWAP_CHAIN_DESC1 sd = { 0 };
	sd.BufferCount = 4;
	sd.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.SampleDesc.Count = 1;
	sd.SampleDesc.Quality = 0;
	sd.SwapEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL;
	sd.Flags = DXGI_SWAP_CHAIN_FLAG_FRAME_LATENCY_WAITABLE_OBJECT;

	ComPtr<IDXGISwapChain1> chain;
	if (FAILED(dxgiFactory->CreateSwapChainForHwnd(device.Get(), gameWindow, &sd, nullptr, nullptr, &chain)))
		throw std::exception("CreateSwapChainForHwnd failed");

	dxgiFactory->MakeWindowAssociation(gameWindow, DXGI_MWA_NO_WINDOW_CHANGES); // Don't allow alt+enter for windowed mode

	chain.As(&swapChain);
	swapChain->SetMaximumFrameLatency(4);
	frameLatencyWaitableObject = swapChain->GetFrameLatencyWaitableObject();
	WaitForSingleObjectEx(frameLatencyWaitableObject, 1000, true);
}

void DirectXDrawer::InitializeDirect2D() {
	D2D1_FACTORY_OPTIONS options = {};

#if defined(_DEBUG)
	options.debugLevel = D2D1_DEBUG_LEVEL_INFORMATION;
#endif

	D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, __uuidof(ID2D1Factory2), &options, &d2dFactory);

	ComPtr<ID2D1Factory1> factory;
	d2dFactory.As(&factory);
	ClearBlackEffect::Register(factory.Get());
}

void DirectXDrawer::InitializeD2DTarget() {
	ComPtr<IDXGISurface> surface;
	swapChain->GetBuffer(0, __uuidof(IDXGISurface), &surface);

	// Create direct2d context
	ComPtr<IDXGIDevice2> dxgiDevice;
	device.As(&dxgiDevice);
	ComPtr<ID2D1Device1> d2dDevice;
	d2dFactory->CreateDevice(dxgiDevice.Get(), &d2dDevice);
	d2dDevice->CreateDeviceContext(D2D1_DEVICE_CONTEXT_OPTIONS_NONE, &d2dContext);

	float dpiX, dpiY;
	d2dFactory->GetDesktopDpi(&dpiX, &dpiY);
	D2D1_BITMAP_PROPERTIES1 targetProperties =
		D2D1::BitmapProperties1(
		D2D1_BITMAP_OPTIONS_TARGET | D2D1_BITMAP_OPTIONS_CANNOT_DRAW,
		D2D1::PixelFormat(DXGI_FORMAT_R8G8B8A8_UNORM, D2D1_ALPHA_MODE_PREMULTIPLIED),
		dpiX, dpiY);

	// Get a D2D surface from the DXGI back buffer to use as the D2D render target.
	d2dContext->CreateBitmapFromDxgiSurface(surface.Get(), &targetProperties, &targetBitmap);
	d2dContext->GetTransform(&defaultTransform);
}

#define RADAR_SIZE_FROM_WIDTH(x)	((x / 6) - ((x / 6) % 4))	// x must be integer (for integer div)
#define CHAT_PADDING_RIGHT			15
#define CHAT_FONT_NAME				TEXT("Consolas")
#define CHAT_FONT_SIZE				16.f
#define CHAT_LINES					6
void DirectXDrawer::InitializeIndependentResources() {
	d2dContext->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Black), &solidColorBrush);

#ifdef USE_CUSTOM_CHAT
	DWriteCreateFactory(
		DWRITE_FACTORY_TYPE_SHARED,
		__uuidof(IDWriteFactory),
		&dwriteFactory);

	dwriteFactory->CreateTextFormat(CHAT_FONT_NAME, NULL,
		DWRITE_FONT_WEIGHT_REGULAR,	DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL,
		CHAT_FONT_SIZE, TEXT("en-us"), &textFormat);

	ComPtr<IDWriteTextLayout> layout;
	int radarSize = RADAR_SIZE_FROM_WIDTH(Detour::Data.gameWidth);
	auto targetSize = targetBitmap->GetSize();
	// chatSize is the size of the "full" chat rendered when the ESC key is pressed
	// we can set this so it doesn't occlude the radar and ESC menu at the top of the screen
	// but personally I don't care about radar and the esc menu being occluded (although it does make arena switching tough)
	/*chatSize = D2D1::SizeF(
		static_cast<float>(targetSize.width - radarSize - CHAT_PADDING_RIGHT),
		targetSize.height * 0.75f);*/
	chatSize = targetSize;

	d2dContext->CreateBitmap(D2D1::SizeU(static_cast<int>(chatSize.width), static_cast<int>(chatSize.height)),
		nullptr, 0, bitmapProps, &chatBitmap);
	
	// Clear the chat bitmap
	d2dContext->BeginDraw();
	d2dContext->SetTarget(chatBitmap.Get());
	d2dContext->Clear(CLEAR_COLOR);
	d2dContext->EndDraw();

	d2dContext->CreateBitmap(D2D1::SizeU(static_cast<int>(chatSize.width), static_cast<int>(chatSize.height)),
		nullptr, 0, bitmapProps, &chatBitmapCopy);

	std::basic_string<TCHAR> sampleText = TEXT("");
	dwriteFactory->CreateTextLayout(sampleText.c_str(), sampleText.size(), textFormat.Get(), chatSize.width, chatSize.height, &layout);
	DWRITE_TEXT_METRICS metrics = {};  layout->GetMetrics(&metrics);
	lineHeight = metrics.height;
#endif
}